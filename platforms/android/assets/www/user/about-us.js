define(['text!user/about-us.html',  '../base/util', "../base/openapi"],
	function(viewTemplate, Util, OpenAPI) {
		return Piece.View.extend({
			id: 'user_about-us',
			events: {
				
			},
			
			/*goMyData: function() {
				var checkLogin = Util.checkLogin();
				if (checkLogin === false) {
					login = new Login();
					login.show();
				} else {
					Backbone.history.navigate("user/user-info", {
						trigger: true
					});
				}
			},*/


			goBackToHome: function() {
				window.history.back();
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var me =this ;
				// alert('about-us');
				console.info(window.document.body.offsetHeight);
				console.info($(document).height());
				// $(".imgContainer").height($(window).height());
				$('#user_about-us').css("height","100%");
				var version = Piece.Store.loadObject('oscAppVersion');
				// console.info(version);
				$(this.el).find("#version").html(version);

				var platformVer = navigator.userAgent;
				
				if(platformVer.indexOf("iPhone")>-1){
					$('body').append("<header class='bar-title'></header>")
					$('.bar-title').append("<div class='left-banner'></div>")
					$('.bar-title').append("<h1 class='title'>关于我们</h1>")
					$('.left-banner').append("<span class='backBtn icon-reply imgsize'></span>")
					// $(".imgContainer").height($(".imgContainer").height()-44);
					// $('.imgContainer').attr('style','margin-top: 44px;');
					$('.backBtn').live("click",function(){
						window.history.back();
					})
				}
			}
	
		
		}); //view define

	});